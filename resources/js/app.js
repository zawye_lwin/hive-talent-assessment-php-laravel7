import Vue from 'vue'
import bootstrap from './bootstrap';

import store from "./store";
import router from './routes'

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('validation-errors', require('./components/ValidationErrors').default);
Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('login', require('./views/Login.vue').default);

const app = new Vue({
    el: '#app',
    store,
    router,
    created() {
        const userInfo = localStorage.getItem('user')
        if (userInfo) {
            const userData = JSON.parse(userInfo)
            this.$store.commit('setUserData', userData)
        }
        axios.interceptors.response.use(
            response => response,
            error => {
                if (error.response.status === 401) {
                    this.$store.dispatch('logout')
                }
                return Promise.reject(error)
            }
        )
    },
});
