import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './views/Login.vue'
import Home from './views/Home.vue'

import ListUsers from './views/Users/ListUsers.vue'
import AddUser from './views/Users/AddUser.vue'
import EditUser from './views/Users/EditUser.vue'

import ListServices from './views/Services/ListServices.vue'
import AddService from './views/Services/AddService.vue'
import EditService from './views/Services/EditService.vue'

import ListCustomers from './views/Customers/ListCustomers.vue'
import AddCustomer from './views/Customers/AddCustomer.vue'
import EditCustomer from './views/Customers/EditCustomer.vue'

import ListBookings from './views/Bookings/ListBookings.vue'
import AddBooking from './views/Bookings/AddBooking.vue'
import EditBooking from './views/Bookings/EditBooking.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/users',
            name: 'list-users',
            component: ListUsers,
            meta: {
                auth: true
            }
        },
        {
            path: '/users/create',
            name: 'add-users',
            component: AddUser,
            meta: {
                auth: true
            }
        },
        {
            path: '/users/edit/:id',
            name: 'edit-users',
            component: EditUser,
            meta: {
                auth: true
            }
        },
        {
            path: '/services',
            name: 'list-services',
            component: ListServices,
            meta: {
                auth: true
            }
        },
        {
            path: '/services/create',
            name: 'add-services',
            component: AddService,
            meta: {
                auth: true
            }
        },
        {
            path: '/services/edit/:id',
            name: 'edit-services',
            component: EditService,
            meta: {
                auth: true
            }
        },
        {
            path: '/customers',
            name: 'list-customers',
            component: ListCustomers,
            meta: {
                auth: true
            }
        },
        {
            path: '/customers/create',
            name: 'add-customers',
            component: AddCustomer,
            meta: {
                auth: true
            }
        },
        {
            path: '/customers/edit/:id',
            name: 'edit-customers',
            component: EditCustomer,
            meta: {
                auth: true
            }
        },
        {
            path: '/bookings',
            name: 'list-bookings',
            component: ListBookings,
            meta: {
                auth: true
            }
        },
        {
            path: '/bookings/create',
            name: 'add-bookings',
            component: AddBooking,
            meta: {
                auth: true
            }
        },
        {
            path: '/bookings/edit/:id',
            name: 'edit-bookings',
            component: EditBooking,
            meta: {
                auth: true
            }
        }

    ]
})

router.beforeEach((to, from, next) => {
    const loggedIn = localStorage.getItem('user')

    if (to.matched.some(record => record.meta.auth) && !loggedIn) {
        next('/login')
        return
    }
    next()
})

export default router