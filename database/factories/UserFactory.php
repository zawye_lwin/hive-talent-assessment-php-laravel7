<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});


$factory->define(App\Service::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomNumber(3),
    ];
});


$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->numerify('092########'),
        'car_number' => $faker->numerify('AA####')
    ];
});


$factory->define(App\Booking::class, function (Faker $faker) {
    return [
        'customer_id' => App\Customer::inRandomOrder()->first(),
        'duration' => $faker->numberBetween($min = 1, $max = 30),
        'price'    => 10,
        'expire_at' => $faker->date(),
        'verification_number' => mt_rand(000001, 999999),
        'notes' => $faker->sentence($nbWords = 6, $variableNbWords = true)
    ];
});
