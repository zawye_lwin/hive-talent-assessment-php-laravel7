<?php

use App\User;
use App\Booking;
use App\Service;
use App\Customer;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Customer::class, 10)->create();
        factory(Booking::class, 2)->create();

        $services = [
            [
                'name'  => 'Basic',
                'price' => 10
            ],
            [
                'name'  => 'Car Wash',
                'price' => 20,
            ],
            [
                'name'  => 'Premium Polish',
                'price' => 30,
            ],
            [
                'name'  => 'Charge battery',
                'price' => 40,
            ]
        ];

        $users = [
            [
                'name'  => 'admin',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ],
            [
                'name'  => 'manager',
                'email' => 'manager@gmail.com',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ]
        ];
        User::insert($users);
        Service::insert($services);
    }
}
