<?php

function success($data, $status)
{
    return response()->json([
        'result'  => 1,
        'message' => 'success',
        'data'    => $data
    ], $status);
}

function fail($message = "fail", $status)
{
    return response()->json([
        'result'  => 0,
        'message' => $message,
        'data'    => []
    ], $status);
}

function successMessage($status)
{
    return response()->json([
        'result'  => 1,
        'message' => 'success'
    ], $status);
}

function failedMessage($status)
{
    return response()->json([
        'result'  => 0,
        'message' => 'failed'
    ], $status);
}
