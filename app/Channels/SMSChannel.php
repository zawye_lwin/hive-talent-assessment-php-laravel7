<?php

namespace App\Channels;

use App\HiveSMS;
use App\Notifications\HiveMessage;
use Illuminate\Notifications\Notification;

class SMSChannel
{

    protected $sms;

    public function __construct(HiveSMS $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {

        if (!$to = $notifiable->routeNotificationFor('hive')) {
            return;
        }
        $message = $notification->toHiveSMS($notifiable);

        if (is_string($message)) {
            $message = new HiveMessage($message);
        }
        $this->sms->send($to, $message->message);
    }
}
