<?php

namespace App\Notifications;

use App\Booking;
use App\Channels\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Notifications\HiveMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingConfirmed extends Notification
{
    use Queueable;

    protected $booking;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SMSChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toHiveSMS($notifiable)
    {
        return (new HiveMessage)->message('Auto Verge service, Dear ' . $notifiable['name'] .
            '. Your booking for car No: ' . $notifiable['car_number'] .
            ' is confirmed and your verification number is: ' . $this->booking->verification_number);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
