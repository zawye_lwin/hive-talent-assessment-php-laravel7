<?php

namespace App\Notifications;

class HiveMessage
{
    public $message;

    public function __construct($message = '')
    {
        $this->message = $message;
    }

    public function message($message)
    {
        $this->message = $message;
        return $this;
    }
}
