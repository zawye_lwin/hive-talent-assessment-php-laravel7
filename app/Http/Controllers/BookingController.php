<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Requests\BookingRequest;
use App\Http\Resources\BookingResource;
use App\Notifications\BookingConfirmed;
use App\Notifications\InvoicePaid;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::where('payment_received', 0)->with('customer')->latest()->paginate();
        $data   = BookingResource::collection($bookings);
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingRequest $request)
    {
        $services = $request->services;
        $price = Service::findMany($services)->sum('price');

        $booking = Booking::create([
            'customer_id'   => $request->customer_id,
            'duration'      => $request->duration,
            'notes'         => $request->notes,
            'price'         => $price,
            'expire_at'     => today()->addDays($request->duration),
            'verification_number' => mt_rand(100000, 999999),
        ]);
        $booking->services()->sync($services);
        $booking->customer->notify(new BookingConfirmed($booking));
        return successMessage(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        return new BookingResource($booking);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(BookingRequest $request, Booking $booking)
    {
        $services = $request->services;
        $price = Service::findMany($services)->sum('price');

        $booking->update([
            'customer_id'   => $request->customer_id,
            'duration'      => $request->duration,
            'notes'         => $request->notes,
            'price'         => $price,
            'expire_at'     => today()->addDays($request->duration)
        ]);
        return successMessage(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();
        return successMessage(200);
    }

    public function checkout(Booking $booking)
    {
        $booking->update(['payment_received' => 1]);
        $booking->customer->notify(new InvoicePaid($booking));
        return successMessage(200);
    }
}
