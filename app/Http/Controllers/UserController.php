<?php

namespace App\Http\Controllers;

use App\User;
use Facade\Ignition\DumpRecorder\DumpHandler;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id', 'name', 'email')->latest()->get();
        return success($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return successMessage(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return success($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return successMessage(201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (auth()->id() != $user->id) {
            $user->delete();
            return successMessage(200);
        }
        return response(["message" => "You Cannot Delete Yourself!"], 403);
    }

    public function resetPassword(Request $request, User $user)
    {
        $this->validate($request, [
            'password'  => 'nullable|confirmed|min:6',
        ]);
        $user->update([
            'password' => bcrypt($request->password)
        ]);
        return successMessage(200);
    }
}
