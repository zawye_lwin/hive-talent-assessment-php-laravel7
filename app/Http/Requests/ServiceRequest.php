<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'price' => ['required', 'numeric'],
        ];

        if ($this->method() == 'POST') {
            $rules += ['name'  => ['required', Rule::unique('services'), 'string', 'max:255']];
        } else {
            $rules += ['name'  => ['required', Rule::unique('services')->ignore($this->service), 'string', 'max:255']];
        }
        return $rules;
    }
}
