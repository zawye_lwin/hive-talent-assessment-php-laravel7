<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'  => ['required', 'string', 'max:255'],
            'phone'  => ['required', 'digits_between:8,13'],
        ];

        if ($this->method() == 'POST') {
            $rules += ['car_number'  => ['required', Rule::unique('customers'), 'string', 'max:255']];
        } else {
            $rules += ['car_number'  => ['required', Rule::unique('customers')->ignore($this->customer), 'string', 'max:255']];
        }
        return $rules;
    }
}
