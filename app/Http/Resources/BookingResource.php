<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"    => $this->id,
            "duration"  => $this->duration,
            "price" => $this->price,
            "expire_at" => $this->expire_at,
            "verification_number"   => $this->verification_number,
            "payment_received" => $this->payment_received,
            "notes" => $this->notes,
            "created_at"    => $this->created_at->toDateString(),
            "customer"  => [
                "id"    => $this->customer->id,
                "name"  => $this->customer->name,
                "phone" => $this->customer->phone,
                "car_number"    => $this->customer->car_number
            ],
            "services" => ServiceResource::collection($this->services()->get())
        ];
    }
}
