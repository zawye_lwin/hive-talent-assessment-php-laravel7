<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthenticationException && $request->wantsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401)
                ->header('Content-Type', 'application/json');
        }

        if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
            return response()->json(['result' => 0, 'message' => 'Requested data not found', 'data' => []], 404)
                ->header('Content-Type', 'application/json');
        }

        if ($exception instanceof NotFoundHttpException && $request->wantsJson()) {
            return response()->json(['result' => 0, 'message' => 'The path does not exists'], 404)
                ->header('Content-Type', 'application/json');
        }

        if ($exception instanceof AuthorizationException && $request->wantsJson()) {
            return response()->json(['result' => 0, 'message' => 'This action is unauthorized.'], 403)
                ->header('Content-Type', 'application/json');
        }
        return parent::render($request, $exception);
    }
}
