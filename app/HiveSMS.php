<?php

namespace App;

use Illuminate\Support\Facades\Log;

class HiveSMS
{
    public function send($phone, $message)
    {
        $data = [
            'phoneNumber' => $phone,
            'message' => $message,
            'username' => 'Auto Verge'
        ];
        try {
            $client  = new \GuzzleHttp\Client;
            $response = $client->request('POST', config('services.sms_service.url') . '/api/messages', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . config('services.sms_service.secret')
                ],
                'json'  => $data

            ]);
            $result = json_decode($response->getBody(), true);
            Log::info($result);
            return $result;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $e->getMessage();
        }
    }
}
