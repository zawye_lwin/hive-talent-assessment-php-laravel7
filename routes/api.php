<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'AuthController@login');


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResource('users',     'UserController');
    Route::apiResource('services',  'ServicesController');
    Route::apiResource('customers', 'CustomerController');

    Route::patch('bookings/{booking}/checkout', 'BookingController@checkout');
    Route::apiResource('bookings',  'BookingController');
});
