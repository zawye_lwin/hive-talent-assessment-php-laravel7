## Prerequiries

- PHP 7
- Composer
- NodeJs
- MySQL

## Installation

- Clone the repository
- Install dependencies with `composer install`
- Install front dependencies with `npm i`
- Copy file `.env.example` as `.env` and edit `.env` file and set your database connection details
- Generate application key with `php artisan key:generate`
- Launch migrations with `php artisan migrate --seed`.
- `php artisan serve` to start the developmet server.
- Build frontend with `npm run dev`

## Configuration
You must set the SMS credentials by updating the `SMS_URL` and `SMS_SECRET` values in the `.env` file.

## Usage
Log as admin with `admin@gmail.com:password` or as user with `manager@gmail.com:password`